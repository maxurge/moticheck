//
//  ViewController.swift
//  MotiCheck
//
//  Created by Kalaiselvi Krishnan on 2/26/19.
//  Copyright © 2019 Kalaiselvi Krishnan. All rights reserved.
//

import UIKit
import CoreMotion

class ViewController: UIViewController {

    @IBOutlet weak var Lbl_Walking: UILabel!
    @IBOutlet weak var Lbl_Station: UILabel!
    @IBOutlet weak var Lbl_Running: UILabel!
    @IBOutlet weak var Lbl_TimeStamp: UILabel!
    @IBOutlet weak var Lbl_Cycling: UILabel!
    @IBOutlet weak var Lbl_Unknown: UILabel!
    @IBOutlet weak var Lbl_Automotive: UILabel!
    @IBOutlet weak var Lbl_Confidence: UILabel!
    @IBOutlet weak var Lbl_SWRACU: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        var x0 = 0, x1 = 0 ,x2 = 0, x3 = 0, x4 = 0, x5 = 0
        
        //Create our activity manager
        let ourActivityManager = CMMotionActivityManager()
        
        if CMMotionActivityManager.isActivityAvailable() {
            ourActivityManager.startActivityUpdates(to: OperationQueue.main) { (motion) in
                self.Lbl_Station.text = (motion?.stationary)! ? "True" : "False"
                x0 = (motion?.stationary)! ? 100000 : 0
                self.Lbl_Walking.text = (motion?.walking)! ? "True" : "False"
                x1 = (motion?.walking)! ? 10000 : 0
                self.Lbl_Running.text = (motion?.running)! ? "True" : "False"
                x2 = (motion?.running)! ? 1000 : 0
                self.Lbl_Automotive.text = (motion?.automotive)! ? "True" : "False"
                x3 = (motion?.automotive)! ? 100 : 0
                self.Lbl_Cycling.text = (motion?.cycling)! ? "True" : "False"
                x4 = (motion?.cycling)! ? 10 : 0
                self.Lbl_Unknown.text = (motion?.unknown)! ? "True" : "False"
                x5 = (motion?.unknown)! ? 1 : 0
                self.Lbl_TimeStamp.text = DateFormatter.localizedString(from: (motion?.startDate) as! Date,dateStyle:DateFormatter.Style.short,timeStyle:DateFormatter.Style.long)

                if motion?.confidence == CMMotionActivityConfidence.low {
                    self.Lbl_Confidence.text = "Low"
                } else if motion?.confidence == CMMotionActivityConfidence.medium {
                    self.Lbl_Confidence.text = "Medium"
                } else if motion?.confidence == CMMotionActivityConfidence.high {
                    self.Lbl_Confidence.text = "High"
                }
                self.Lbl_SWRACU.text = String(x0+x1+x2+x3+x4+x5);
            }
            }
    
    }
    
}
